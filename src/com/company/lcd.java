package com.company;

public class lcd {

    private String[] cableChoice = {"DVA","DVI", "HDMI"};
    private String status, cable;
    private int volume;
    private int brightness;


    public lcd(){
        this.status = "mati";
        this.cable = "VGA";
        this.volume = 18;
        this.brightness = 65;
    }

    public void turnon(){
        this.status = "nyala";
    }
    public void turnoff(){
        this.status = "mati";
    }
    public void freeze(){
        this.status = "freeze";
    }
    public void volumeup(){
        this.volume++;
    }
    public void volumedown(){
        this.volume--;
    }
    public void setVolume(int volume){
        this.volume = volume;
    }
    public void Brightnessup(){
        this.brightness++;
    }
    public void Brightnessdown(){
        this.brightness--;
    }
    public void setBrightness(int brightness){
        this.brightness = brightness;
    }
    public void cableup(){
        for (int i = 0; i < cableChoice.length; i++) {
            if(cable.equals(cableChoice[i])){
                if(i==cableChoice.length-1){
                    this.cable=cableChoice[0];
                }else{
                    this.cable=cableChoice[i+1];
                }
            }
        }
    }
    public void cabledown(){
        for (int i = 0; i < cableChoice.length; i++) {
            if(cable.equals(cableChoice[i])){
                if(i==0){
                    this.cable=cableChoice[2];
                }else{
                    this.cable=cableChoice[i-1];
                }
            }
        }
    }
    public void setCable(String cable){
        this.cable=cable;
    }
    public void DisplayLcd(){
        System.out.println("========== LCD ==========");
        System.out.println("Status     : "+status);
        System.out.println("Volume     : "+volume);
        System.out.println("Brightness : "+brightness);
        System.out.println("Cable      : "+cable);
        System.out.println("=========================");
    }
}


